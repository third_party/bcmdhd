/*
 * Broadcom SPI Low-Level Hardware Driver API
 *
 * Copyright 1999-2016, Broadcom Corporation
 * All rights reserved,
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *
 * This software is provided by the copyright holder "as is" and any express or
 * implied warranties, including, but not limited to, the implied warranties of
 * merchantability and fitness for a particular purpose are disclaimed. In no event
 * shall copyright holder be liable for any direct, indirect, incidental, special,
 * exemplary, or consequential damages (including, but not limited to, procurement
 * of substitute goods or services; loss of use, data, or profits; or business
 * interruption) however caused and on any theory of liability, whether in
 * contract, strict liability, or tort (including negligence or otherwise) arising
 * in any way out of the use of this software, even if advised of the possibility
 * of such damage
 *
 *
 * <<Broadcom-WL-IPTag/Open:>>
 *
 * $Id: bcmspi.h 514727 2014-11-12 03:02:48Z $
 */
#ifndef	_BCM_SPI_H
#define	_BCM_SPI_H

extern void spi_devintr_off(sdioh_info_t *sd);
extern void spi_devintr_on(sdioh_info_t *sd);
extern bool spi_start_clock(sdioh_info_t *sd, uint16 new_sd_divisor);
extern bool spi_controller_highspeed_mode(sdioh_info_t *sd, bool hsmode);
extern bool spi_check_client_intr(sdioh_info_t *sd, int *is_dev_intr);
extern bool spi_hw_attach(sdioh_info_t *sd);
extern bool spi_hw_detach(sdioh_info_t *sd);
extern void spi_sendrecv(sdioh_info_t *sd, uint8 *msg_out, uint8 *msg_in, int msglen);
extern void spi_spinbits(sdioh_info_t *sd);
extern void spi_waitbits(sdioh_info_t *sd, bool yield);

#endif /* _BCM_SPI_H */
