/* Crossdriver (bcmdhd/brcmfmac) symbols extracted from bcmdhd include/proto/802.11.h.
 *
 * Copyright 1999-2016, Broadcom Corporation
 * All rights reserved,
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *
 * This software is provided by the copyright holder "as is" and any express or
 * implied warranties, including, but not limited to, the implied warranties of
 * merchantability and fitness for a particular purpose are disclaimed. In no event
 * shall copyright holder be liable for any direct, indirect, incidental, special,
 * exemplary, or consequential damages (including, but not limited to, procurement
 * of substitute goods or services; loss of use, data, or profits; or business
 * interruption) however caused and on any theory of liability, whether in
 * contract, strict liability, or tort (including negligence or otherwise) arising
 * in any way out of the use of this software, even if advised of the possibility
 * of such damage
 */

#ifndef THIRD_PARTY_BCMDHD_CROSSDRIVER_INCLUDE_PROTO_802_11_H_
#define THIRD_PARTY_BCMDHD_CROSSDRIVER_INCLUDE_PROTO_802_11_H_

#include <zircon/compiler.h>

/* WME Access Category Indices (ACIs) */
#define AC_BE 0    /* Best Effort */
#define AC_BK 1    /* Background */
#define AC_VI 2    /* Video */
#define AC_VO 3    /* Voice */
#define AC_COUNT 4 /* number of ACs */

struct edcf_acparam {
  uint8_t aci;
  uint8_t ecw;
  uint16_t txop; /* stored in network order (ls octet first) */
} __PACKED;
using edcf_acparam_t = struct edcf_acparam;

/* ACI */
#define EDCF_AIFSN_MIN 1     /* AIFSN minimum value */
#define EDCF_AIFSN_MAX 15    /* AIFSN maximum value */
#define EDCF_AIFSN_MASK 0x0f /* AIFSN mask */
#define EDCF_ACM_MASK 0x10   /* ACM mask */
#define EDCF_ACI_MASK 0x60   /* ACI mask */
#define EDCF_ACI_SHIFT 5     /* ACI shift */
#define EDCF_AIFSN_SHIFT 12  /* 4 MSB(0xFFF) in ifs_ctl for AC idx */

/* ECW */
#define EDCF_ECW_MIN 0  /* cwmin/cwmax exponent minimum value */
#define EDCF_ECW_MAX 15 /* cwmin/cwmax exponent maximum value */
#define EDCF_ECW2CW(exp) ((1 << (exp)) - 1)
#define EDCF_ECWMIN_MASK 0x0f /* cwmin exponent form mask */
#define EDCF_ECWMAX_MASK 0xf0 /* cwmax exponent form mask */
#define EDCF_ECWMAX_SHIFT 4   /* cwmax exponent form shift */

/* TXOP */
#define EDCF_TXOP_MIN 0     /* TXOP minimum value */
#define EDCF_TXOP_MAX 65535 /* TXOP maximum value */
#define EDCF_TXOP2USEC(txop) ((txop) << 5)

#endif  // THIRD_PARTY_BCMDHD_CROSSDRIVER_INCLUDE_PROTO_802_11_H_
