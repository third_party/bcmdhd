/* Crossdriver (bcmdhd/brcmfmac) symbols extracted from bcmdhd include/devctrl_if/wlioctl_defs.h
 *
 * Copyright 1999-2016, Broadcom Corporation
 * All rights reserved,
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *
 * This software is provided by the copyright holder "as is" and any express or
 * implied warranties, including, but not limited to, the implied warranties of
 * merchantability and fitness for a particular purpose are disclaimed. In no event
 * shall copyright holder be liable for any direct, indirect, incidental, special,
 * exemplary, or consequential damages (including, but not limited to, procurement
 * of substitute goods or services; loss of use, data, or profits; or business
 * interruption) however caused and on any theory of liability, whether in
 * contract, strict liability, or tort (including negligence or otherwise) arising
 * in any way out of the use of this software, even if advised of the possibility
 * of such damage
 */

#ifndef THIRD_PARTY_BCMDHD_CROSSDRIVER_INCLUDE_DEVCTRL_IF_WLIOCTL_DEFS_H_
#define THIRD_PARTY_BCMDHD_CROSSDRIVER_INCLUDE_DEVCTRL_IF_WLIOCTL_DEFS_H_

/* WL_RSPEC defines for rate information */
#define WL_RSPEC_RATE_MASK 0x000000FF    /* rate or HT MCS value */
#define WL_RSPEC_VHT_MCS_MASK 0x0000000F /* VHT MCS value */
#define WL_RSPEC_VHT_NSS_MASK 0x000000F0 /* VHT Nss value */
#define WL_RSPEC_VHT_NSS_SHIFT 4         /* VHT Nss value shift */
#define WL_RSPEC_TXEXP_MASK 0x00000300
#define WL_RSPEC_TXEXP_SHIFT 8
#define WL_RSPEC_BW_MASK 0x00070000       /* bandwidth mask */
#define WL_RSPEC_BW_SHIFT 16              /* bandwidth shift */
#define WL_RSPEC_STBC 0x00100000          /* STBC encoding, Nsts = 2 x Nss */
#define WL_RSPEC_TXBF 0x00200000          /* bit indicates TXBF mode */
#define WL_RSPEC_LDPC 0x00400000          /* bit indicates adv coding in use */
#define WL_RSPEC_SGI 0x00800000           /* Short GI mode */
#define WL_RSPEC_ENCODING_MASK 0x03000000 /* Encoding of Rate/MCS field */
#define WL_RSPEC_OVERRIDE_RATE 0x40000000 /* bit indicate to override mcs only */
#define WL_RSPEC_OVERRIDE_MODE 0x80000000 /* bit indicates override both rate & mode */

/* WL_RSPEC_ENCODING field defs */
#define WL_RSPEC_ENCODE_RATE 0x00000000 /* Legacy rate is stored in RSPEC_RATE_MASK */
#define WL_RSPEC_ENCODE_HT 0x01000000   /* HT MCS is stored in RSPEC_RATE_MASK */
#define WL_RSPEC_ENCODE_VHT 0x02000000  /* VHT MCS and Nss is stored in RSPEC_RATE_MASK */

/* WL_RSPEC_BW field defs */
#define WL_RSPEC_BW_UNSPECIFIED 0
#define WL_RSPEC_BW_20MHZ 0x00010000
#define WL_RSPEC_BW_40MHZ 0x00020000
#define WL_RSPEC_BW_80MHZ 0x00030000
#define WL_RSPEC_BW_160MHZ 0x00040000
#define WL_RSPEC_BW_10MHZ 0x00050000
#define WL_RSPEC_BW_5MHZ 0x00060000
#define WL_RSPEC_BW_2P5MHZ 0x00070000

/* WNM/NPS subfeatures mask */
#define WL_WNM_BSSTRANS 0x00000001
#define WL_WNM_PROXYARP 0x00000002
#define WL_WNM_MAXIDLE 0x00000004
#define WL_WNM_TIMBC 0x00000008
#define WL_WNM_TFS 0x00000010
#define WL_WNM_SLEEP 0x00000020
#define WL_WNM_DMS 0x00000040
#define WL_WNM_FMS 0x00000080
#define WL_WNM_NOTIF 0x00000100
#define WL_WNM_MAX 0x00000200

#endif  // THIRD_PARTY_BCMDHD_CROSSDRIVER_INCLUDE_DEVCTRL_IF_WLIOCTL_DEFS_H_
