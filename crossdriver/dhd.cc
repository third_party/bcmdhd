/* Crossdriver (bcmdhd/brcmfmac) logic extracted from bcmdhd dhd.h externs.
 *
 * Copyright 1999-2016, Broadcom Corporation
 * All rights reserved,
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *
 * This software is provided by the copyright holder "as is" and any express or
 * implied warranties, including, but not limited to, the implied warranties of
 * merchantability and fitness for a particular purpose are disclaimed. In no event
 * shall copyright holder be liable for any direct, indirect, incidental, special,
 * exemplary, or consequential damages (including, but not limited to, procurement
 * of substitute goods or services; loss of use, data, or profits; or business
 * interruption) however caused and on any theory of liability, whether in
 * contract, strict liability, or tort (including negligence or otherwise) arising
 * in any way out of the use of this software, even if advised of the possibility
 * of such damage
 */

#include "dhd.h"

#include <stdint.h>

#include <memory>

#include "include/devctrl_if/wlioctl_defs.h"
#include "wl_cfg80211.h"

bool rspec_to_rate_info(uint32_t rspec, rate_info_t* ri) {
  uint32_t encode;
  uint32_t rate;

  if (rspec == 0) {
    return false;
  }
  encode = (rspec & WL_RSPEC_ENCODING_MASK);
  rate = (rspec & WL_RSPEC_RATE_MASK);

  ri->sgi = ((rspec & WL_RSPEC_SGI) != 0) ? 1 : 0;

  switch (rspec & WL_RSPEC_BW_MASK) {
    case WL_RSPEC_BW_20MHZ:
      ri->bw = RATE_INFO_BW_20_MHZ;
      break;
    case WL_RSPEC_BW_40MHZ:
      ri->bw = RATE_INFO_BW_40_MHZ;
      break;
    case WL_RSPEC_BW_80MHZ:
      ri->bw = RATE_INFO_BW_80_MHZ;
      break;
    case WL_RSPEC_BW_160MHZ:
    default:
      ri->bw = RATE_INFO_BW_160_MHZ;
      break;
  }

  if (encode == WL_RSPEC_ENCODE_RATE) {
    ri->type = RATE_LEGACY;
    switch (rate) {
      case 2:
        ri->idx.b = 0;
        break;
      case 4:
        ri->idx.b = 1;
        break;
      case 11:
        ri->idx.b = 2;
        break;
      case 22:
        ri->idx.b = 3;
        break;
      case 12:
        ri->idx.g = 0;
        break;
      case 18:
        ri->idx.g = 1;
        break;
      case 24:
        ri->idx.g = 2;
        break;
      case 36:
        ri->idx.g = 3;
        break;
      case 48:
        ri->idx.g = 4;
        break;
      case 72:
        ri->idx.g = 5;
        break;
      case 96:
        ri->idx.g = 6;
        break;
      case 104:
      default:
        ri->idx.g = 7;
        break;
    }
  } else if (encode == WL_RSPEC_ENCODE_HT) {
    ri->type = RATE_HT;
    ri->idx.mcs = rate % 16;

    if (rate <= 7) {
      ri->nss = 1;
    } else if (rate <= 15) {
      ri->nss = 2;
    } else if (rate <= 23) {
      ri->nss = 3;
    } else {
      ri->nss = 4;
    }
  } else {
    ri->type = RATE_VHT;
    ri->idx.vhtmcs = (rspec & WL_RSPEC_VHT_MCS_MASK);
    ri->nss = ((rspec & WL_RSPEC_VHT_NSS_MASK) >> WL_RSPEC_VHT_NSS_SHIFT);
  }

  return true;
}

bool get_histograms(wl_wstats_cnt_t wl_stats_cnt, chanspec_t chanspec, uint32_t version,
                    uint32_t rxchain, histograms_report_t* out_report) {
  if (wl_stats_cnt.version > WSTATS_CNT_T_VERSION) {
    // Unexpected wl_stats_cnt version.
    return false;
  }

  const chanspec_t real_chanspec = interpret_chanspec(chanspec, version);
  if (real_chanspec == INVCHANSPEC) {
    return false;
  }

  int band = chanspec & WL_CHANSPEC_BAND_MASK;
  AntennaId antenna_id;
  antenna_id.freq = (band == WL_CHANSPEC_BAND_5G) ? ANTENNA_5G : ANTENNA_2G;
  antenna_id.idx = (rxchain == 1) ? 0 : 1;

  std::memset(out_report, 0, sizeof(histograms_report_t));
  std::memcpy(out_report->rxsnr, wl_stats_cnt.rxsnr, sizeof(out_report->rxsnr));
  std::memcpy(out_report->rxnoiseflr, wl_stats_cnt.rxnoiseflr, sizeof(out_report->rxnoiseflr));
  std::memcpy(out_report->rxrssi, wl_stats_cnt.rxrssi, sizeof(out_report->rxrssi));
  std::memcpy(out_report->rx11b, wl_stats_cnt.rx11b, sizeof(out_report->rx11b));
  std::memcpy(out_report->rx11g, wl_stats_cnt.rx11g, sizeof(out_report->rx11g));
  std::memcpy(out_report->rx11n, wl_stats_cnt.rx11n, sizeof(out_report->rx11n));
  std::memcpy(out_report->rx11ac, wl_stats_cnt.rx11ac, sizeof(out_report->rx11ac));
  std::memcpy(&out_report->antennaid, &antenna_id, sizeof(out_report->antennaid));
  return true;
}
