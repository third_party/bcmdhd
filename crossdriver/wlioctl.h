/* Crossdriver (bcmdhd/brcmfmac) symbols extracted from bcmdhd include/wlioctl.h.
 *
 * Copyright 1999-2016, Broadcom Corporation
 * All rights reserved,
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *
 * This software is provided by the copyright holder "as is" and any express or
 * implied warranties, including, but not limited to, the implied warranties of
 * merchantability and fitness for a particular purpose are disclaimed. In no event
 * shall copyright holder be liable for any direct, indirect, incidental, special,
 * exemplary, or consequential damages (including, but not limited to, procurement
 * of substitute goods or services; loss of use, data, or profits; or business
 * interruption) however caused and on any theory of liability, whether in
 * contract, strict liability, or tort (including negligence or otherwise) arising
 * in any way out of the use of this software, even if advised of the possibility
 * of such damage
 */

#ifndef THIRD_PARTY_BCMDHD_CROSSDRIVER_WLIOCTL_H_
#define THIRD_PARTY_BCMDHD_CROSSDRIVER_WLIOCTL_H_

#include "include/proto/802.11.h"

typedef uint32_t uint32;
typedef uint16_t uint16;

/* used for reassociation/roam to a specific BSSID and channel */
using wl_reassoc_params_t = struct reassoc_params {
  uint8_t bssid[ETH_ALEN];    /**< 00:00:00:00:00:00: broadcast scan */
  uint16_t bssid_cnt;         /**< 0: use chanspec_num, and the single bssid,
                               * otherwise count of chanspecs in chanspec_list
                               * AND paired bssids following chanspec_list
                               * also, chanspec_num has to be set to zero
                               * for bssid list to be used
                               */
  int32_t chanspec_num;       /**< 0: all available channels,
                               * otherwise count of chanspecs in chanspec_list
                               */
  uint16_t chanspec_list[1];  /**< list of chanspecs */
};

using event_msgs_ext_command_t = enum event_msgs_ext_command {
  EVENTMSGS_NONE = 0,
  EVENTMSGS_SET_BIT = 1,
  EVENTMSGS_RESET_BIT = 2,
  EVENTMSGS_SET_MASK = 3
};

#define EVENTMSGS_VERSION 1
#define EVENTMSGS_EXT_STRUCT_SIZE offsetof(eventmsgs_ext_t, event_mask)

/**
 * struct eventmsgs_ext - firmware event message.
 *
 * @version: version information.
 * @command: the command of this iovar.
 * @length: for SET it would be mask size from the application to the firmware for GET it would be
 * actual firmware mask size.
 * @max_get_size: only for iovar get, indicating the max mask length of the event mask.
 * @event_mask: the event mask bits.
 */
using eventmsgs_ext_t = struct eventmsgs_ext {
  uint8_t version;
  uint8_t command;
  uint8_t length;
  uint8_t max_get_size;
  uint8_t event_mask[1];
};

#define ASSOC_MGR_CURRENT_VERSION 0x0

#define ASSOC_MGR_CMD_PAUSE_ON_EVT 0 /* have assoc pause on certain events */
#define ASSOC_MGR_CMD_ABORT_ASSOC 1
#define ASSOC_MGR_CMD_SEND_AUTH 3

#define ASSOC_MGR_PARAMS_EVENT_NONE 0 /* use this to resume as well as clear */
#define ASSOC_MGR_PARAMS_PAUSE_EVENT_AUTH_RESP 1

/**
 * struct assoc_mgr_cmd - Command struct to control firmware association command process.
 *
 * @version: Version of this command.
 * @length: Length of data that the firmware need to deal with.
 * @cmd: Command to control the association process.
 * @params: Parameters for the commands.
 */
using assoc_mgr_cmd_t = struct assoc_mgr_cmd {
  uint16_t version;
  uint16_t length;
  uint16_t cmd;
  uint16_t params;
};

#define NFIFO 6 /**< # tx/rx fifopairs */
#define NREINITREASONCOUNT 8
typedef struct {
  uint16 version; /* see definition of WL_CNT_T_VERSION */
  uint16 length;  /* length of entire structure */

  /* transmit stat counters */
  uint32 txframe;   /* tx data frames */
  uint32 txbyte;    /* tx data bytes */
  uint32 txretrans; /* tx mac retransmits */
  uint32 txerror;   /* tx data errors (derived: sum of others) */
  uint32 txctl;     /* tx management frames */
  uint32 txprshort; /* tx short preamble frames */
  uint32 txserr;    /* tx status errors */
  uint32 txnobuf;   /* tx out of buffers errors */
  uint32 txnoassoc; /* tx discard because we're not associated */
  uint32 txrunt;    /* tx runt frames */
  uint32 txchit;    /* tx header cache hit (fastpath) */
  uint32 txcmiss;   /* tx header cache miss (slowpath) */

  /* transmit chip error counters */
  uint32 txuflo;   /* tx fifo underflows */
  uint32 txphyerr; /* tx phy errors (indicated in tx status) */
  uint32 txphycrs;

  /* receive stat counters */
  uint32 rxframe;     /* rx data frames */
  uint32 rxbyte;      /* rx data bytes */
  uint32 rxerror;     /* rx data errors (derived: sum of others) */
  uint32 rxctl;       /* rx management frames */
  uint32 rxnobuf;     /* rx out of buffers errors */
  uint32 rxnondata;   /* rx non data frames in the data channel errors */
  uint32 rxbadds;     /* rx bad DS errors */
  uint32 rxbadcm;     /* rx bad control or management frames */
  uint32 rxfragerr;   /* rx fragmentation errors */
  uint32 rxrunt;      /* rx runt frames */
  uint32 rxgiant;     /* rx giant frames */
  uint32 rxnoscb;     /* rx no scb error */
  uint32 rxbadproto;  /* rx invalid frames */
  uint32 rxbadsrcmac; /* rx frames with Invalid Src Mac */
  uint32 rxbadda;     /* rx frames tossed for invalid da */
  uint32 rxfilter;    /* rx frames filtered out */

  /* receive chip error counters */
  uint32 rxoflo;        /* rx fifo overflow errors */
  uint32 rxuflo[NFIFO]; /* rx dma descriptor underflow errors */

  uint32 d11cnt_txrts_off;   /* d11cnt txrts value when reset d11cnt */
  uint32 d11cnt_rxcrc_off;   /* d11cnt rxcrc value when reset d11cnt */
  uint32 d11cnt_txnocts_off; /* d11cnt txnocts value when reset d11cnt */

  /* misc counters */
  uint32 dmade; /* tx/rx dma descriptor errors */
  uint32 dmada; /* tx/rx dma data errors */
  uint32 dmape; /* tx/rx dma descriptor protocol errors */
  uint32 reset; /* reset count */
  uint32 tbtt;  /* cnts the TBTT int's */
  uint32 txdmawar;
  uint32 pkt_callback_reg_fail; /* callbacks register failure */

  /* MAC counters: 32-bit version of d11.h's macstat_t */
  uint32 txallfrm;        /* total number of frames sent, incl. Data, ACK, RTS, CTS,
                           * Control Management (includes retransmissions)
                           */
  uint32 txrtsfrm;        /* number of RTS sent out by the MAC */
  uint32 txctsfrm;        /* number of CTS sent out by the MAC */
  uint32 txackfrm;        /* number of ACK frames sent out */
  uint32 txdnlfrm;        /* Not used */
  uint32 txbcnfrm;        /* beacons transmitted */
  uint32 txfunfl[6];      /* per-fifo tx underflows */
  uint32 rxtoolate;       /* receive too late */
  uint32 txfbw;           /* transmit at fallback bw (dynamic bw) */
  uint32 txtplunfl;       /* Template underflows (mac was too slow to transmit ACK/CTS
                           * or BCN)
                           */
  uint32 txphyerror;      /* Transmit phy error, type of error is reported in tx-status for
                           * driver enqueued frames
                           */
  uint32 rxfrmtoolong;    /* Received frame longer than legal limit (2346 bytes) */
  uint32 rxfrmtooshrt;    /* Received frame did not contain enough bytes for its frame type */
  uint32 rxinvmachdr;     /* Either the protocol version != 0 or frame type not
                           * data/control/management
                           */
  uint32 rxbadfcs;        /* number of frames for which the CRC check failed in the MAC */
  uint32 rxbadplcp;       /* parity check of the PLCP header failed */
  uint32 rxcrsglitch;     /* PHY was able to correlate the preamble but not the header */
  uint32 rxstrt;          /* Number of received frames with a good PLCP
                           * (i.e. passing parity check)
                           */
  uint32 rxdfrmucastmbss; /* Number of received DATA frames with good FCS and matching RA */
  uint32 rxmfrmucastmbss; /* number of received mgmt frames with good FCS and matching RA */
  uint32 rxcfrmucast;     /* number of received CNTRL frames with good FCS and matching RA */
  uint32 rxrtsucast;      /* number of unicast RTS addressed to the MAC (good FCS) */
  uint32 rxctsucast;      /* number of unicast CTS addressed to the MAC (good FCS) */
  uint32 rxackucast;      /* number of ucast ACKS received (good FCS) */
  uint32 rxdfrmocast;     /* number of received DATA frames (good FCS and not matching RA) */
  uint32 rxmfrmocast;     /* number of received MGMT frames (good FCS and not matching RA) */
  uint32 rxcfrmocast;     /* number of received CNTRL frame (good FCS and not matching RA) */
  uint32 rxrtsocast;      /* number of received RTS not addressed to the MAC */
  uint32 rxctsocast;      /* number of received CTS not addressed to the MAC */
  uint32 rxdfrmmcast;     /* number of RX Data multicast frames received by the MAC */
  uint32 rxmfrmmcast;     /* number of RX Management multicast frames received by the MAC */
  uint32 rxcfrmmcast;     /* number of RX Control multicast frames received by the MAC
                           * (unlikely to see these)
                           */
  uint32 rxbeaconmbss;    /* beacons received from member of BSS */
  uint32 rxdfrmucastobss; /* number of unicast frames addressed to the MAC from
                           * other BSS (WDS FRAME)
                           */
  uint32 rxbeaconobss;    /* beacons received from other BSS */
  uint32 rxrsptmout;      /* Number of response timeouts for transmitted frames
                           * expecting a response
                           */
  uint32 bcntxcancl;      /* transmit beacons canceled due to receipt of beacon (IBSS) */
  uint32 rxf0ovfl;        /* Number of receive fifo 0 overflows */
  uint32 rxf1ovfl;        /* Number of receive fifo 1 overflows (obsolete) */
  uint32 rxf2ovfl;        /* Number of receive fifo 2 overflows (obsolete) */
  uint32 txsfovfl;        /* Number of transmit status fifo overflows (obsolete) */
  uint32 pmqovfl;         /* Number of PMQ overflows */
  uint32 rxcgprqfrm;      /* Number of received Probe requests that made it into
                           * the PRQ fifo
                           */
  uint32 rxcgprsqovfl;    /* Rx Probe Request Que overflow in the AP */
  uint32 txcgprsfail;     /* Tx Probe Response Fail. AP sent probe response but did
                           * not get ACK
                           */
  uint32 txcgprssuc;      /* Tx Probe Response Success (ACK was received) */
  uint32 prs_timeout;     /* Number of probe requests that were dropped from the PRQ
                           * fifo because a probe response could not be sent out within
                           * the time limit defined in M_PRS_MAXTIME
                           */
  uint32 rxnack;
  uint32 frmscons;
  uint32 txnack; /* obsolete */
  uint32 rxback; /* blockack rxcnt */
  uint32 txback; /* blockack txcnt */

  /* 802.11 MIB counters, pp. 614 of 802.11 reaff doc. */
  uint32 txfrag;   /* dot11TransmittedFragmentCount */
  uint32 txmulti;  /* dot11MulticastTransmittedFrameCount */
  uint32 txfail;   /* dot11FailedCount */
  uint32 txretry;  /* dot11RetryCount */
  uint32 txretrie; /* dot11MultipleRetryCount */
  uint32 rxdup;    /* dot11FrameduplicateCount */
  uint32 txrts;    /* dot11RTSSuccessCount */
  uint32 txnocts;  /* dot11RTSFailureCount */
  uint32 txnoack;  /* dot11ACKFailureCount */
  uint32 rxfrag;   /* dot11ReceivedFragmentCount */
  uint32 rxmulti;  /* dot11MulticastReceivedFrameCount */
  uint32 rxcrc;    /* dot11FCSErrorCount */
  uint32 txfrmsnt; /* dot11TransmittedFrameCount (bogus MIB?) */
  uint32 rxundec;  /* dot11WEPUndecryptableCount */

  /* WPA2 counters (see rxundec for DecryptFailureCount) */
  uint32 tkipmicfaill; /* TKIPLocalMICFailures */
  uint32 tkipcntrmsr;  /* TKIPCounterMeasuresInvoked */
  uint32 tkipreplay;   /* TKIPReplays */
  uint32 ccmpfmterr;   /* CCMPFormatErrors */
  uint32 ccmpreplay;   /* CCMPReplays */
  uint32 ccmpundec;    /* CCMPDecryptErrors */
  uint32 fourwayfail;  /* FourWayHandshakeFailures */
  uint32 wepundec;     /* dot11WEPUndecryptableCount */
  uint32 wepicverr;    /* dot11WEPICVErrorCount */
  uint32 decsuccess;   /* DecryptSuccessCount */
  uint32 tkipicverr;   /* TKIPICVErrorCount */
  uint32 wepexcluded;  /* dot11WEPExcludedCount */

  uint32 rxundec_mcst; /* dot11WEPUndecryptableCount */

  /* WPA2 counters (see rxundec for DecryptFailureCount) */
  uint32 tkipmicfaill_mcst; /* TKIPLocalMICFailures */
  uint32 tkipcntrmsr_mcst;  /* TKIPCounterMeasuresInvoked */
  uint32 tkipreplay_mcst;   /* TKIPReplays */
  uint32 ccmpfmterr_mcst;   /* CCMPFormatErrors */
  uint32 ccmpreplay_mcst;   /* CCMPReplays */
  uint32 ccmpundec_mcst;    /* CCMPDecryptErrors */
  uint32 fourwayfail_mcst;  /* FourWayHandshakeFailures */
  uint32 wepundec_mcst;     /* dot11WEPUndecryptableCount */
  uint32 wepicverr_mcst;    /* dot11WEPICVErrorCount */
  uint32 decsuccess_mcst;   /* DecryptSuccessCount */
  uint32 tkipicverr_mcst;   /* TKIPICVErrorCount */
  uint32 wepexcluded_mcst;  /* dot11WEPExcludedCount */

  uint32 txchanrej;   /* Tx frames suppressed due to channel rejection */
  uint32 txexptime;   /* Tx frames suppressed due to timer expiration */
  uint32 psmwds;      /* Count PSM watchdogs */
  uint32 phywatchdog; /* Count Phy watchdogs (triggered by ucode) */

  /* MBSS counters, AP only */
  uint32 prq_entries_handled;         /* PRQ entries read in */
  uint32 prq_undirected_entries;      /*    which were bcast bss & ssid */
  uint32 prq_bad_entries;             /*    which could not be translated to info */
  uint32 atim_suppress_count;         /* TX suppressions on ATIM fifo */
  uint32 bcn_template_not_ready;      /* Template marked in use on send bcn ... */
  uint32 bcn_template_not_ready_done; /* ...but "DMA done" interrupt rcvd */
  uint32 late_tbtt_dpc;               /* TBTT DPC did not happen in time */

  /* per-rate receive stat counters */
  uint32 rx1mbps;   /* packets rx at 1Mbps */
  uint32 rx2mbps;   /* packets rx at 2Mbps */
  uint32 rx5mbps5;  /* packets rx at 5.5Mbps */
  uint32 rx6mbps;   /* packets rx at 6Mbps */
  uint32 rx9mbps;   /* packets rx at 9Mbps */
  uint32 rx11mbps;  /* packets rx at 11Mbps */
  uint32 rx12mbps;  /* packets rx at 12Mbps */
  uint32 rx18mbps;  /* packets rx at 18Mbps */
  uint32 rx24mbps;  /* packets rx at 24Mbps */
  uint32 rx36mbps;  /* packets rx at 36Mbps */
  uint32 rx48mbps;  /* packets rx at 48Mbps */
  uint32 rx54mbps;  /* packets rx at 54Mbps */
  uint32 rx108mbps; /* packets rx at 108mbps */
  uint32 rx162mbps; /* packets rx at 162mbps */
  uint32 rx216mbps; /* packets rx at 216 mbps */
  uint32 rx270mbps; /* packets rx at 270 mbps */
  uint32 rx324mbps; /* packets rx at 324 mbps */
  uint32 rx378mbps; /* packets rx at 378 mbps */
  uint32 rx432mbps; /* packets rx at 432 mbps */
  uint32 rx486mbps; /* packets rx at 486 mbps */
  uint32 rx540mbps; /* packets rx at 540 mbps */

  /* pkteng rx frame stats */
  uint32 pktengrxducast; /* unicast frames rxed by the pkteng code */
  uint32 pktengrxdmcast; /* multicast frames rxed by the pkteng code */

  uint32 rfdisable;        /* count of radio disables */
  uint32 bphy_rxcrsglitch; /* PHY count of bphy glitches */
  uint32 bphy_badplcp;

  uint32 txmpdu_sgi;  /* count for sgi transmit */
  uint32 rxmpdu_sgi;  /* count for sgi received */
  uint32 txmpdu_stbc; /* count for stbc transmit */
  uint32 rxmpdu_stbc; /* count for stbc received */

  uint32 rxdrop20s; /* drop secondary cnt */
} wl_cnt_ver_6_t;

typedef struct {
  uint16 version; /**< see definition of WL_CNT_T_VERSION */
  uint16 length;  /**< length of entire structure */

  /* transmit stat counters */
  uint32 txframe;   /**< tx data frames */
  uint32 txbyte;    /**< tx data bytes */
  uint32 txretrans; /**< tx mac retransmits */
  uint32 txerror;   /**< tx data errors (derived: sum of others) */
  uint32 txctl;     /**< tx management frames */
  uint32 txprshort; /**< tx short preamble frames */
  uint32 txserr;    /**< tx status errors */
  uint32 txnobuf;   /**< tx out of buffers errors */
  uint32 txnoassoc; /**< tx discard because we're not associated */
  uint32 txrunt;    /**< tx runt frames */
  uint32 txchit;    /**< tx header cache hit (fastpath) */
  uint32 txcmiss;   /**< tx header cache miss (slowpath) */

  /* transmit chip error counters */
  uint32 txuflo;   /**< tx fifo underflows */
  uint32 txphyerr; /**< tx phy errors (indicated in tx status) */
  uint32 txphycrs;

  /* receive stat counters */
  uint32 rxframe;     /**< rx data frames */
  uint32 rxbyte;      /**< rx data bytes */
  uint32 rxerror;     /**< rx data errors (derived: sum of others) */
  uint32 rxctl;       /**< rx management frames */
  uint32 rxnobuf;     /**< rx out of buffers errors */
  uint32 rxnondata;   /**< rx non data frames in the data channel errors */
  uint32 rxbadds;     /**< rx bad DS errors */
  uint32 rxbadcm;     /**< rx bad control or management frames */
  uint32 rxfragerr;   /**< rx fragmentation errors */
  uint32 rxrunt;      /**< rx runt frames */
  uint32 rxgiant;     /**< rx giant frames */
  uint32 rxnoscb;     /**< rx no scb error */
  uint32 rxbadproto;  /**< rx invalid frames */
  uint32 rxbadsrcmac; /**< rx frames with Invalid Src Mac */
  uint32 rxbadda;     /**< rx frames tossed for invalid da */
  uint32 rxfilter;    /**< rx frames filtered out */

  /* receive chip error counters */
  uint32 rxoflo;        /**< rx fifo overflow errors */
  uint32 rxuflo[NFIFO]; /**< rx dma descriptor underflow errors */

  uint32 d11cnt_txrts_off;   /**< d11cnt txrts value when reset d11cnt */
  uint32 d11cnt_rxcrc_off;   /**< d11cnt rxcrc value when reset d11cnt */
  uint32 d11cnt_txnocts_off; /**< d11cnt txnocts value when reset d11cnt */

  /* misc counters */
  uint32 dmade; /**< tx/rx dma descriptor errors */
  uint32 dmada; /**< tx/rx dma data errors */
  uint32 dmape; /**< tx/rx dma descriptor protocol errors */
  uint32 reset; /**< reset count */
  uint32 tbtt;  /**< cnts the TBTT int's */
  uint32 txdmawar;
  uint32 pkt_callback_reg_fail; /**< callbacks register failure */

  /* MAC counters: 32-bit version of d11.h's macstat_t */
  uint32 txallfrm;        /**< total number of frames sent, incl. Data, ACK, RTS, CTS,
                           * Control Management (includes retransmissions)
                           */
  uint32 txrtsfrm;        /**< number of RTS sent out by the MAC */
  uint32 txctsfrm;        /**< number of CTS sent out by the MAC */
  uint32 txackfrm;        /**< number of ACK frames sent out */
  uint32 txdnlfrm;        /**< Not used */
  uint32 txbcnfrm;        /**< beacons transmitted */
  uint32 txfunfl[6];      /**< per-fifo tx underflows */
  uint32 rxtoolate;       /**< receive too late */
  uint32 txfbw;           /**< transmit at fallback bw (dynamic bw) */
  uint32 txtplunfl;       /**< Template underflows (mac was too slow to transmit ACK/CTS
                           * or BCN)
                           */
  uint32 txphyerror;      /**< Transmit phy error, type of error is reported in tx-status for
                           * driver enqueued frames
                           */
  uint32 rxfrmtoolong;    /**< Received frame longer than legal limit (2346 bytes) */
  uint32 rxfrmtooshrt;    /**< Received frame did not contain enough bytes for its frame type */
  uint32 rxinvmachdr;     /**< Either the protocol version != 0 or frame type not
                           * data/control/management
                           */
  uint32 rxbadfcs;        /**< number of frames for which the CRC check failed in the MAC */
  uint32 rxbadplcp;       /**< parity check of the PLCP header failed */
  uint32 rxcrsglitch;     /**< PHY was able to correlate the preamble but not the header */
  uint32 rxstrt;          /**< Number of received frames with a good PLCP
                           * (i.e. passing parity check)
                           */
  uint32 rxdfrmucastmbss; /* Number of received DATA frames with good FCS and matching RA */
  uint32 rxmfrmucastmbss; /* number of received mgmt frames with good FCS and matching RA */
  uint32 rxcfrmucast;     /**< number of received CNTRL frames with good FCS and matching RA */
  uint32 rxrtsucast;      /**< number of unicast RTS addressed to the MAC (good FCS) */
  uint32 rxctsucast;      /**< number of unicast CTS addressed to the MAC (good FCS) */
  uint32 rxackucast;      /**< number of ucast ACKS received (good FCS) */
  uint32 rxdfrmocast;     /**< number of received DATA frames (good FCS and not matching RA) */
  uint32 rxmfrmocast;     /**< number of received MGMT frames (good FCS and not matching RA) */
  uint32 rxcfrmocast;     /**< number of received CNTRL frame (good FCS and not matching RA) */
  uint32 rxrtsocast;      /**< number of received RTS not addressed to the MAC */
  uint32 rxctsocast;      /**< number of received CTS not addressed to the MAC */
  uint32 rxdfrmmcast;     /**< number of RX Data multicast frames received by the MAC */
  uint32 rxmfrmmcast;     /**< number of RX Management multicast frames received by the MAC */
  uint32 rxcfrmmcast;     /**< number of RX Control multicast frames received by the MAC
                           * (unlikely to see these)
                           */
  uint32 rxbeaconmbss;    /**< beacons received from member of BSS */
  uint32 rxdfrmucastobss; /* number of unicast frames addressed to the MAC from
                           * other BSS (WDS FRAME)
                           */
  uint32 rxbeaconobss;    /**< beacons received from other BSS */
  uint32 rxrsptmout;      /**< Number of response timeouts for transmitted frames
                           * expecting a response
                           */
  uint32 bcntxcancl;      /**< transmit beacons canceled due to receipt of beacon (IBSS) */
  uint32 rxf0ovfl;        /**< Number of receive fifo 0 overflows */
  uint32 rxf1ovfl;        /**< Number of receive fifo 1 overflows (obsolete) */
  uint32 rxf2ovfl;        /**< Number of receive fifo 2 overflows (obsolete) */
  uint32 txsfovfl;        /**< Number of transmit status fifo overflows (obsolete) */
  uint32 pmqovfl;         /**< Number of PMQ overflows */
  uint32 rxcgprqfrm;      /**< Number of received Probe requests that made it into
                           * the PRQ fifo
                           */
  uint32 rxcgprsqovfl;    /**< Rx Probe Request Que overflow in the AP */
  uint32 txcgprsfail;     /**< Tx Probe Response Fail. AP sent probe response but did
                           * not get ACK
                           */
  uint32 txcgprssuc;      /**< Tx Probe Response Success (ACK was received) */
  uint32 prs_timeout;     /**< Number of probe requests that were dropped from the PRQ
                           * fifo because a probe response could not be sent out within
                           * the time limit defined in M_PRS_MAXTIME
                           */
  uint32 rxnack;          /**< obsolete */
  uint32 frmscons;        /**< obsolete */
  uint32 txnack;          /**< obsolete */
  uint32 rxback;          /**< blockack rxcnt */
  uint32 txback;          /**< blockack txcnt */

  /* 802.11 MIB counters, pp. 614 of 802.11 reaff doc. */
  uint32 txfrag;   /**< dot11TransmittedFragmentCount */
  uint32 txmulti;  /**< dot11MulticastTransmittedFrameCount */
  uint32 txfail;   /**< dot11FailedCount */
  uint32 txretry;  /**< dot11RetryCount */
  uint32 txretrie; /**< dot11MultipleRetryCount */
  uint32 rxdup;    /**< dot11FrameduplicateCount */
  uint32 txrts;    /**< dot11RTSSuccessCount */
  uint32 txnocts;  /**< dot11RTSFailureCount */
  uint32 txnoack;  /**< dot11ACKFailureCount */
  uint32 rxfrag;   /**< dot11ReceivedFragmentCount */
  uint32 rxmulti;  /**< dot11MulticastReceivedFrameCount */
  uint32 rxcrc;    /**< dot11FCSErrorCount */
  uint32 txfrmsnt; /**< dot11TransmittedFrameCount (bogus MIB?) */
  uint32 rxundec;  /**< dot11WEPUndecryptableCount */

  /* WPA2 counters (see rxundec for DecryptFailureCount) */
  uint32 tkipmicfaill; /**< TKIPLocalMICFailures */
  uint32 tkipcntrmsr;  /**< TKIPCounterMeasuresInvoked */
  uint32 tkipreplay;   /**< TKIPReplays */
  uint32 ccmpfmterr;   /**< CCMPFormatErrors */
  uint32 ccmpreplay;   /**< CCMPReplays */
  uint32 ccmpundec;    /**< CCMPDecryptErrors */
  uint32 fourwayfail;  /**< FourWayHandshakeFailures */
  uint32 wepundec;     /**< dot11WEPUndecryptableCount */
  uint32 wepicverr;    /**< dot11WEPICVErrorCount */
  uint32 decsuccess;   /**< DecryptSuccessCount */
  uint32 tkipicverr;   /**< TKIPICVErrorCount */
  uint32 wepexcluded;  /**< dot11WEPExcludedCount */

  uint32 txchanrej;   /**< Tx frames suppressed due to channel rejection */
  uint32 psmwds;      /**< Count PSM watchdogs */
  uint32 phywatchdog; /**< Count Phy watchdogs (triggered by ucode) */

  /* MBSS counters, AP only */
  uint32 prq_entries_handled;         /**< PRQ entries read in */
  uint32 prq_undirected_entries;      /**<    which were bcast bss & ssid */
  uint32 prq_bad_entries;             /**<    which could not be translated to info */
  uint32 atim_suppress_count;         /**< TX suppressions on ATIM fifo */
  uint32 bcn_template_not_ready;      /**< Template marked in use on send bcn ... */
  uint32 bcn_template_not_ready_done; /* ...but "DMA done" interrupt rcvd */
  uint32 late_tbtt_dpc;               /**< TBTT DPC did not happen in time */

  /* per-rate receive stat counters */
  uint32 rx1mbps;   /* packets rx at 1Mbps */
  uint32 rx2mbps;   /* packets rx at 2Mbps */
  uint32 rx5mbps5;  /* packets rx at 5.5Mbps */
  uint32 rx6mbps;   /* packets rx at 6Mbps */
  uint32 rx9mbps;   /* packets rx at 9Mbps */
  uint32 rx11mbps;  /* packets rx at 11Mbps */
  uint32 rx12mbps;  /* packets rx at 12Mbps */
  uint32 rx18mbps;  /* packets rx at 18Mbps */
  uint32 rx24mbps;  /* packets rx at 24Mbps */
  uint32 rx36mbps;  /* packets rx at 36Mbps */
  uint32 rx48mbps;  /* packets rx at 48Mbps */
  uint32 rx54mbps;  /* packets rx at 54Mbps */
  uint32 rx108mbps; /* packets rx at 108mbps */
  uint32 rx162mbps; /* packets rx at 162mbps */
  uint32 rx216mbps; /* packets rx at 216 mbps */
  uint32 rx270mbps; /* packets rx at 270 mbps */
  uint32 rx324mbps; /* packets rx at 324 mbps */
  uint32 rx378mbps; /* packets rx at 378 mbps */
  uint32 rx432mbps; /* packets rx at 432 mbps */
  uint32 rx486mbps; /* packets rx at 486 mbps */
  uint32 rx540mbps; /* packets rx at 540 mbps */

  /* pkteng rx frame stats */
  uint32 pktengrxducast; /* unicast frames rxed by the pkteng code */
  uint32 pktengrxdmcast; /* multicast frames rxed by the pkteng code */

  uint32 rfdisable;        /**< count of radio disables */
  uint32 bphy_rxcrsglitch; /**< PHY count of bphy glitches */
  uint32 bphy_badplcp;

  uint32 txexptime; /**< Tx frames suppressed due to timer expiration */

  uint32 txmpdu_sgi;  /**< count for sgi transmit */
  uint32 rxmpdu_sgi;  /**< count for sgi received */
  uint32 txmpdu_stbc; /**< count for stbc transmit */
  uint32 rxmpdu_stbc; /**< count for stbc received */

  uint32 rxundec_mcst; /**< dot11WEPUndecryptableCount */

  /* WPA2 counters (see rxundec for DecryptFailureCount) */
  uint32 tkipmicfaill_mcst; /**< TKIPLocalMICFailures */
  uint32 tkipcntrmsr_mcst;  /**< TKIPCounterMeasuresInvoked */
  uint32 tkipreplay_mcst;   /**< TKIPReplays */
  uint32 ccmpfmterr_mcst;   /**< CCMPFormatErrors */
  uint32 ccmpreplay_mcst;   /**< CCMPReplays */
  uint32 ccmpundec_mcst;    /**< CCMPDecryptErrors */
  uint32 fourwayfail_mcst;  /**< FourWayHandshakeFailures */
  uint32 wepundec_mcst;     /**< dot11WEPUndecryptableCount */
  uint32 wepicverr_mcst;    /**< dot11WEPICVErrorCount */
  uint32 decsuccess_mcst;   /**< DecryptSuccessCount */
  uint32 tkipicverr_mcst;   /**< TKIPICVErrorCount */
  uint32 wepexcluded_mcst;  /**< dot11WEPExcludedCount */

  uint32 dma_hang; /**< count for dma hang */
  uint32 reinit;   /**< count for reinit */

  uint32 pstatxucast;   /**< count of ucast frames xmitted on all psta assoc */
  uint32 pstatxnoassoc; /**< count of txnoassoc frames xmitted on all psta assoc */
  uint32 pstarxucast;   /**< count of ucast frames received on all psta assoc */
  uint32 pstarxbcmc;    /**< count of bcmc frames received on all psta */
  uint32 pstatxbcmc;    /**< count of bcmc frames transmitted on all psta */

  uint32 cso_passthrough;                  /* hw cso required but passthrough */
  uint32 cso_normal;                       /**< hw cso hdr for normal process */
  uint32 chained;                          /**< number of frames chained */
  uint32 chainedsz1;                       /**< number of chain size 1 frames */
  uint32 unchained;                        /**< number of frames not chained */
  uint32 maxchainsz;                       /**< max chain size so far */
  uint32 currchainsz;                      /**< current chain size */
  uint32 rxdrop20s;                        /**< drop secondary cnt */
  uint32 pciereset;                        /**< Secondary Bus Reset issued by driver */
  uint32 cfgrestore;                       /**< configspace restore by driver */
  uint32 reinitreason[NREINITREASONCOUNT]; /* reinitreason counters; 0: Unknown reason */
  uint32 rxrtry;                           /**< num of received packets with retry bit on */
  uint32 txmpdu;    /**< macstat cnt only valid in ver 11. number of MPDUs txed.  */
  uint32 rxnodelim; /**< macstat cnt only valid in ver 11.
                     * number of occasions that no valid delimiter is detected
                     * by ampdu parser.
                     */
  uint32 rxmpdu_mu; /* Number of MU MPDUs received */

  /* detailed control/management frames */
  uint32 txbar;        /**< Number of TX BAR */
  uint32 rxbar;        /**< Number of RX BAR */
  uint32 txpspoll;     /**< Number of TX PS-poll */
  uint32 rxpspoll;     /**< Number of RX PS-poll */
  uint32 txnull;       /**< Number of TX NULL_DATA */
  uint32 rxnull;       /**< Number of RX NULL_DATA */
  uint32 txqosnull;    /**< Number of TX NULL_QoSDATA */
  uint32 rxqosnull;    /**< Number of RX NULL_QoSDATA */
  uint32 txassocreq;   /**< Number of TX ASSOC request */
  uint32 rxassocreq;   /**< Number of RX ASSOC request */
  uint32 txreassocreq; /**< Number of TX REASSOC request */
  uint32 rxreassocreq; /**< Number of RX REASSOC request */
  uint32 txdisassoc;   /**< Number of TX DISASSOC */
  uint32 rxdisassoc;   /**< Number of RX DISASSOC */
  uint32 txassocrsp;   /**< Number of TX ASSOC response */
  uint32 rxassocrsp;   /**< Number of RX ASSOC response */
  uint32 txreassocrsp; /**< Number of TX REASSOC response */
  uint32 rxreassocrsp; /**< Number of RX REASSOC response */
  uint32 txauth;       /**< Number of TX AUTH */
  uint32 rxauth;       /**< Number of RX AUTH */
  uint32 txdeauth;     /**< Number of TX DEAUTH */
  uint32 rxdeauth;     /**< Number of RX DEAUTH */
  uint32 txprobereq;   /**< Number of TX probe request */
  uint32 rxprobereq;   /**< Number of RX probe request */
  uint32 txprobersp;   /**< Number of TX probe response */
  uint32 rxprobersp;   /**< Number of RX probe response */
  uint32 txaction;     /**< Number of TX action frame */
  uint32 rxaction;     /**< Number of RX action frame */

} wl_cnt_ver_11_t;

typedef struct {
  uint32 packets;
  uint32 bytes;
} wl_traffic_stats_t;

typedef struct {
  uint16 version; /* see definition of WL_WME_CNT_VERSION */
  uint16 length;  /* length of entire structure */

  wl_traffic_stats_t tx[AC_COUNT];        /* Packets transmitted */
  wl_traffic_stats_t tx_failed[AC_COUNT]; /* Packets dropped or failed to transmit */
  wl_traffic_stats_t rx[AC_COUNT];        /* Packets received */
  wl_traffic_stats_t rx_failed[AC_COUNT]; /* Packets failed to receive */

  wl_traffic_stats_t forward[AC_COUNT]; /* Packets forwarded by AP */

  wl_traffic_stats_t tx_expired[AC_COUNT]; /* packets dropped due to lifetime expiry */

} wl_wme_cnt_t;

/* "wnm_bsstrans_policy" argument programs behavior after BSSTRANS Req reception.
 * BSS-Transition feature is used by multiple programs such as NPS-PF, VE-PF,
 * Band-steering, Hotspot 2.0 and customer requirements. Each PF and its test plan
 * mandates different behavior on receiving BSS-transition request. To accomodate
 * such divergent behaviors these policies have been created.
 */
enum {
  WL_BSSTRANS_POLICY_ROAM_ALWAYS = 0,  /* Roam (or disassociate) in all cases */
  WL_BSSTRANS_POLICY_ROAM_IF_MODE = 1, /* Roam only if requested by Request Mode field */
  WL_BSSTRANS_POLICY_ROAM_IF_PREF = 2, /* Roam only if Preferred BSS provided */
  WL_BSSTRANS_POLICY_WAIT = 3,         /* Wait for deauth and send Accepted status */
  WL_BSSTRANS_POLICY_PRODUCT = 4,      /* Policy for real product use cases (non-pf) */
};

#endif  // THIRD_PARTY_BCMDHD_CROSSDRIVER_WLIOCTL_H_
