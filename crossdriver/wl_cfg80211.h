/* Crossdriver (bcmdhd/brcmfmac) symbols extracted from bcmdhd wl_cfg80211.h.
 *
 * Copyright 1999-2016, Broadcom Corporation
 * All rights reserved,
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *
 * This software is provided by the copyright holder "as is" and any express or
 * implied warranties, including, but not limited to, the implied warranties of
 * merchantability and fitness for a particular purpose are disclaimed. In no event
 * shall copyright holder be liable for any direct, indirect, incidental, special,
 * exemplary, or consequential damages (including, but not limited to, procurement
 * of substitute goods or services; loss of use, data, or profits; or business
 * interruption) however caused and on any theory of liability, whether in
 * contract, strict liability, or tort (including negligence or otherwise) arising
 * in any way out of the use of this software, even if advised of the possibility
 * of such damage
 */

#ifndef THIRD_PARTY_BCMDHD_CROSSDRIVER_WL_CFG80211_H_
#define THIRD_PARTY_BCMDHD_CROSSDRIVER_WL_CFG80211_H_

#include "bcmwifi_channels.h"

// Return a new chanspec given a legacy chanspec.
// Ported from wl_chspec_from_legacy.
chanspec_t chspec_from_legacy(chanspec_t legacy_chspec);

// Convert firmware chanspec from legacy chanspec if necessary.
// Ported from wl_chspec_driver_to_host.
chanspec_t interpret_chanspec(chanspec_t chanspec, uint32_t version);

#endif  // THIRD_PARTY_BCMDHD_CROSSDRIVER_WL_CFG80211_H_
