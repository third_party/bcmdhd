/* Crossdriver (bcmdhd/brcmfmac) symbols extracted from bcmdhd dhd.h.
 *
 * Copyright 1999-2016, Broadcom Corporation
 * All rights reserved,
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *
 * This software is provided by the copyright holder "as is" and any express or
 * implied warranties, including, but not limited to, the implied warranties of
 * merchantability and fitness for a particular purpose are disclaimed. In no event
 * shall copyright holder be liable for any direct, indirect, incidental, special,
 * exemplary, or consequential damages (including, but not limited to, procurement
 * of substitute goods or services; loss of use, data, or profits; or business
 * interruption) however caused and on any theory of liability, whether in
 * contract, strict liability, or tort (including negligence or otherwise) arising
 * in any way out of the use of this software, even if advised of the possibility
 * of such damage
 */

#ifndef THIRD_PARTY_BCMDHD_CROSSDRIVER_DHD_H_
#define THIRD_PARTY_BCMDHD_CROSSDRIVER_DHD_H_

#include <stdint.h>

#include "bcmwifi_channels.h"

enum { RATE_LEGACY, RATE_HT, RATE_VHT };

// rate_info_t bw field uses these values.
enum {
  RATE_INFO_BW_20_MHZ,
  RATE_INFO_BW_40_MHZ,
  RATE_INFO_BW_80_MHZ,
  RATE_INFO_BW_160_MHZ,
};

using rate_info_t = struct {
  uint8_t type;
  union {
    uint8_t b;
    uint8_t g;
    uint8_t mcs;
    uint8_t vhtmcs;
  } idx;
  uint8_t nss;
  uint8_t bw;
  uint8_t sgi;
};

#define WSTATS_CNT_T_VERSION 1
#define WSTATS_RATE_RANGE_11B 4
#define WSTATS_RATE_RANGE_11G 8
#define WSTATS_SGI_RANGE 2
#define WSTATS_BW_RANGE_11N 2
#define WSTATS_MCS_RANGE_11N 16
#define WSTATS_BW_RANGE_11AC 3
#define WSTATS_NSS_RANGE 2
#define WSTATS_MCS_RANGE_11AC 10
#define WSTATS_SNR_RANGE 256
#define WSTATS_NOISE_FLR_RANGE 256
#define WSTATS_RSSI_RANGE 256

struct wl_wstats_cnt {
  uint16_t version;
  uint16_t length;

  /* pkt counters per snr */
  uint32_t rxsnr[WSTATS_SNR_RANGE];
  /* pkt counters per noise floor */
  uint32_t rxnoiseflr[WSTATS_NOISE_FLR_RANGE];
  /* pkt counters per signal level */
  uint32_t rxrssi[WSTATS_RSSI_RANGE];
  /* [RATE 1,2,55,11] */
  uint32_t rx11b[WSTATS_RATE_RANGE_11B];
  /* [RATE 6,9,12,18,24,36,48,54] */
  uint32_t rx11g[WSTATS_RATE_RANGE_11G];
  /* [SGI off/on][BW 20/40][MCS 0-15] */
  uint32_t rx11n[WSTATS_SGI_RANGE][WSTATS_BW_RANGE_11N][WSTATS_MCS_RANGE_11N];
  /* [NSS 1/2][SGI off/on][BW 20/40/80][MCS 0-9] */
  uint32_t rx11ac[WSTATS_NSS_RANGE][WSTATS_SGI_RANGE][WSTATS_BW_RANGE_11AC][WSTATS_MCS_RANGE_11AC];
};

using wl_wstats_cnt_t = wl_wstats_cnt;

enum AntennaFreq {
  ANTENNA_2G,
  ANTENNA_5G,
  NUM_ANTENNA_FREQ,
};

struct AntennaId {
  int freq;
  int idx;
};

struct histograms_report {
  struct AntennaId antennaid;

  /* pkt counters per snr */
  uint32_t rxsnr[WSTATS_SNR_RANGE];
  /* pkt counters per noise floor */
  uint32_t rxnoiseflr[WSTATS_NOISE_FLR_RANGE];
  /* pkt counters per signal level */
  uint32_t rxrssi[WSTATS_RSSI_RANGE];
  /* [RATE 1,2,55,11]: 0-3 */
  uint32_t rx11b[WSTATS_RATE_RANGE_11B];
  /* [RATE 6,9,12,18,24,36,48,54]: 4-11 */
  uint32_t rx11g[WSTATS_RATE_RANGE_11G];
  /* [SGI off/on][BW 20/40][MCS 0-15]: 12-75 */
  uint32_t rx11n[WSTATS_SGI_RANGE][WSTATS_BW_RANGE_11N][WSTATS_MCS_RANGE_11N];
  /* [NSS 1/2][SGI off/on][BW 20/40/80][MCS 0-9]: */
  uint32_t rx11ac[WSTATS_NSS_RANGE][WSTATS_SGI_RANGE][WSTATS_BW_RANGE_11AC][WSTATS_MCS_RANGE_11AC];
};

using histograms_report_t = histograms_report;

// Decipher a ratespec. Returns false if ratespec == 0, true otherwise.
// Ported from dhd_rspec_to_rate_info in dhd_linux.c.
bool rspec_to_rate_info(uint32_t rspec, rate_info_t* ri);

// Construct a histograms_report_t from the given arguments.
// Ported from dhd_dev_get_histograms in dhd_linux.c.
bool get_histograms(wl_wstats_cnt_t wl_stats_cnt, chanspec_t chanspec, uint32_t version,
                    uint32_t rxchain, histograms_report_t* out_report);

#endif  // THIRD_PARTY_BCMDHD_CROSSDRIVER_DHD_H_
