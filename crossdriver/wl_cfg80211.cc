/* Crossdriver (bcmdhd/brcmfmac) logic extracted from bcmdhd wl_cfg80211.c.
 *
 * Copyright 1999-2016, Broadcom Corporation
 * All rights reserved,
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *
 * This software is provided by the copyright holder "as is" and any express or
 * implied warranties, including, but not limited to, the implied warranties of
 * merchantability and fitness for a particular purpose are disclaimed. In no event
 * shall copyright holder be liable for any direct, indirect, incidental, special,
 * exemplary, or consequential damages (including, but not limited to, procurement
 * of substitute goods or services; loss of use, data, or profits; or business
 * interruption) however caused and on any theory of liability, whether in
 * contract, strict liability, or tort (including negligence or otherwise) arising
 * in any way out of the use of this software, even if advised of the possibility
 * of such damage
 */

#include "wl_cfg80211.h"

#include "bcmwifi_channels.h"

chanspec_t chspec_from_legacy(chanspec_t legacy_chspec) {
  chanspec_t chspec;
  /* get the channel number */
  chspec = LCHSPEC_CHANNEL(legacy_chspec);

  /* convert the band */
  if (LCHSPEC_IS2G(legacy_chspec)) {
    chspec |= WL_CHANSPEC_BAND_2G;
  } else {
    chspec |= WL_CHANSPEC_BAND_5G;
  }

  /* convert the bw and sideband */
  if (LCHSPEC_IS20(legacy_chspec)) {
    chspec |= WL_CHANSPEC_BW_20;
  } else {
    chspec |= WL_CHANSPEC_BW_40;
    if (LCHSPEC_CTL_SB(legacy_chspec) == WL_LCHANSPEC_CTL_SB_LOWER) {
      chspec |= WL_CHANSPEC_CTL_SB_L;
    } else {
      chspec |= WL_CHANSPEC_CTL_SB_U;
    }
  }

  if (chspec_malformed(chspec)) {
    return INVCHANSPEC;
  }
  return chspec;
}

chanspec_t interpret_chanspec(chanspec_t chanspec, uint32_t version) {
  if (version == 1) {
    return chspec_from_legacy(chanspec);
  }
  return chanspec;
}
