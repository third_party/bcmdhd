/*
 * Broadcom Dongle Host Driver (DHD), CSI
 *
 * Copyright 1999-2016, Broadcom Corporation
 * All rights reserved,
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *
 * This software is provided by the copyright holder "as is" and any express or
 * implied warranties, including, but not limited to, the implied warranties of
 * merchantability and fitness for a particular purpose are disclaimed. In no event
 * shall copyright holder be liable for any direct, indirect, incidental, special,
 * exemplary, or consequential damages (including, but not limited to, procurement
 * of substitute goods or services; loss of use, data, or profits; or business
 * interruption) however caused and on any theory of liability, whether in
 * contract, strict liability, or tort (including negligence or otherwise) arising
 * in any way out of the use of this software, even if advised of the possibility
 * of such damage
 *
 * $Id: dhd_csi.h 558438 2015-05-22 06:05:11Z $
 */
#ifndef __DHD_CSI_H__
#define __DHD_CSI_H__

/* Maxinum csi file dump size */
#define MAX_CSI_FILESZ		(32 * 1024)
/* Maxinum subcarrier number */
#define MAXIMUM_CFR_DATA	256
#define CSI_DUMP_PATH		"/sys/bcm-dhd"
#define UEVENT_LENGTH		IFNAMSIZ + 7 /* IFNAMESIZE + 1 + 6(IFACE=) */

typedef struct cfr_dump_header {
	/* Peer MAC address for which CFR data was captured*/
	uint8 peer_macaddr[6];
	/* 0 - CFR capture successful; 1 - CFR capture not successful */
	uint8 cfr_capture_status;
	/* 0 – 20MHz, 1 – 40MHz, 2 – 80MHz, 3 – 160MHz, 4 – 80+80MHz */
	uint8 cfr_capture_bw;
	/* 0 – 20MHz, 1 – 40MHz, 2 – 80MHz, 3 – 160MHz, 4 – 80+80MHz */
	uint8 cfr_channel_bw;
	/* 0 for b mode, 1 for g mode, 2 for n mode, 3 for ac mode */
	uint8 phy_mode;
	/* primary 20MHz channel frequency */
	uint16 channel;
	/* center frequency 1 in MHz */
	uint16 band_cent_freq1;
	/* center frequency 2 in MHz (Valid for 11ac vht 80plus80 mode) */
	uint16 band_cent_freq2;
	/* Capture mode, 0 – Legacy, 1 – Duplicate Legacy, 2 – HT, 3 – VHT */
	uint8 cfr_capture_mode;
	/* Capture type, 0 – Null Frame */
	uint8 cfr_capture_type;
	/* STS (Space Time Streams) count */
	uint8 sts;
	/* Number of Rx chains */
	uint8 num_rx;
	/* Number of subcarriers/tones */
	uint16 num_carrier;
	/* Number of bits per tone (per I/Q). Minimum is 8 and maximum is 16. */
	uint8 num_bits;
	/* Chip id. 1 for BCM43458 */
	uint8 chip_id;
	/* Time stamp when CFR capture is taken, in microseconds since the epoch */
	uint64 cfr_timestamp;
	/* Length of the CFR dump in units of bytes, excluding the header size */
	uint32 cfr_dump_length;
} __attribute__((packed)) cfr_dump_header_t;

typedef struct cfr_dump_data {
	cfr_dump_header_t header;
	uint32 data[MAXIMUM_CFR_DATA];
} cfr_dump_data_t;

typedef struct {
	struct list_head list;
	cfr_dump_data_t entry;
} cfr_dump_list_t;

int dhd_csi_event_handler(dhd_pub_t *dhd, wl_event_msg_t *event, void *event_data);

int dhd_csi_init(dhd_pub_t *dhd);

int dhd_csi_deinit(dhd_pub_t *dhd);

void dhd_csi_clean_list(dhd_pub_t *dhd);

int dhd_csi_dump_list(dhd_pub_t *dhd, char *buf, size_t count, int ifidx);
void remove_csi_list(dhd_pub_t *dhd, struct list_head *csi_list);
#endif /* __DHD_CSI_H__ */
